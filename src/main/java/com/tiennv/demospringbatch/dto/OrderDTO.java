package com.tiennv.demospringbatch.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderDTO {
    private Long id;

    private Long customerId;

    private Long itemId;

    private String itemName;

    private Double itemPrice;

    private String purchaseDate;
}
