//package com.tiennv.demospringbatch.jobs;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.tiennv.demospringbatch.dto.OrderDTO;
//import com.tiennv.demospringbatch.entity.Order;
//import com.tiennv.demospringbatch.repository.OrderRepository;
//import lombok.RequiredArgsConstructor;
//import org.springframework.batch.core.Job;
//import org.springframework.batch.core.Step;
//import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
//import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
//import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
//import org.springframework.batch.core.launch.support.RunIdIncrementer;
//import org.springframework.batch.item.ItemProcessor;
//import org.springframework.batch.item.ItemReader;
//import org.springframework.batch.item.ItemWriter;
//import org.springframework.batch.item.data.RepositoryItemWriter;
//import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
//import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.io.ClassPathResource;
//
//import java.time.Instant;
//
//@Configuration
//@EnableBatchProcessing
//@RequiredArgsConstructor
//public class BatchConfiguration {
//
//    private final JobBuilderFactory jobBuilderFactory;
//    private final StepBuilderFactory stepBuilderFactory;
//    private final OrderRepository orderRepository;
//    private final ObjectMapper objectMapper;
//
//
//    @Value("${url-file}")
//    private String resourceFile;
//
//    @Value("${create-by}")
//    private String createBy;
//
//    @Bean
//    public ItemReader<OrderDTO> read() {
//        return new FlatFileItemReaderBuilder<OrderDTO>()
//                .name("orderItemReader")
//                .resource(new ClassPathResource(resourceFile))
//                .delimited()
//                .names(new String[] {"CustomerId", "ItemId", "ItemName", "ItemPrice", "PurchaseDate"})
//                .fieldSetMapper(new BeanWrapperFieldSetMapper<>() {{
//                    setTargetType(OrderDTO.class);
//                }})
//                .build();
//    }
//
//    @Bean
//    public ItemProcessor<OrderDTO, Order> process() {
//
//        return (orderDTO) -> Order.builder()
//                .customerId(orderDTO.getCustomerId())
//                .itemId(orderDTO.getItemId())
//                .itemName(orderDTO.getItemName())
//                .itemPrice(orderDTO.getItemPrice())
//                .purchaseDate(orderDTO.getPurchaseDate())
//                .createBy(createBy)
//                .createDate(Instant.now())
//                .build();
//    }
//
//
//    @Bean
//    public ItemWriter<Order> writer() {
//        RepositoryItemWriter<Order> writer = new RepositoryItemWriter<>();
//        writer.setRepository(orderRepository);
//        writer.setMethodName("save");
//        return writer;
//    }
//
//    @Bean
//    public Job importOrderJob() {
//        return jobBuilderFactory.get("importOrderJob")
//                .incrementer(new RunIdIncrementer())
//                .listener(new JobCompletionNotificationListener())
//                .flow(step1())
//                .end()
//                .build();
//    }
//
//    @Bean
//    public Step step1() {
//        return stepBuilderFactory.get("step1")
//                .<OrderDTO, Order> chunk(10)
//                .reader(read())
//                .processor(process())
//                .writer(writer())
//                .build();
//    }
//}
