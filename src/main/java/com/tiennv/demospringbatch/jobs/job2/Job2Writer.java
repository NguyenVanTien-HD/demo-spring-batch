package com.tiennv.demospringbatch.jobs.job2;

import com.tiennv.demospringbatch.entity.Order;
import com.tiennv.demospringbatch.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

@RequiredArgsConstructor
@Slf4j
public class Job2Writer implements ItemWriter<Order> {

    private final OrderRepository orderRepository;

    @Override
    public void write(List<? extends Order> list) throws Exception {
        log.info("Save all {} order", list.size());
        orderRepository.saveAll(list);
    }
}
