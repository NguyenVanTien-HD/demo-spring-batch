package com.tiennv.demospringbatch.jobs.job2;

import com.tiennv.demospringbatch.dto.OrderDTO;
import com.tiennv.demospringbatch.entity.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.batch.item.ItemProcessor;

import java.time.Instant;

@RequiredArgsConstructor
public class Job2Processor implements ItemProcessor<OrderDTO, Order> {

    private final String createBy;

    @Override
    public Order process(OrderDTO orderDTO) {
        return Order.builder()
                .customerId(orderDTO.getCustomerId())
                .itemId(orderDTO.getItemId())
                .itemName(orderDTO.getItemName())
                .itemPrice(orderDTO.getItemPrice())
                .purchaseDate(orderDTO.getPurchaseDate())
                .createBy(createBy)
                .createDate(Instant.now())
                .build();
    }
}
