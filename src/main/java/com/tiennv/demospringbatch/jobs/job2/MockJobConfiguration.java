package com.tiennv.demospringbatch.jobs.job2;

import com.tiennv.demospringbatch.TermDB;
import com.tiennv.demospringbatch.dto.OrderDTO;
import com.tiennv.demospringbatch.entity.Order;
import com.tiennv.demospringbatch.jobs.JobCompletionNotificationListener;
import com.tiennv.demospringbatch.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Date;
import java.util.List;

@Slf4j
@Configuration
@RequiredArgsConstructor
@EnableBatchProcessing
public class MockJobConfiguration implements Runnable {

    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;
    private final OrderRepository orderRepository;
    private final JobLauncher jobLauncher;

    @Value("${create-by}")
    private String createBy;

    @Bean
    public Job mockJob2() {
        return jobBuilderFactory.get("importOrderJob")
                .incrementer(new RunIdIncrementer())
                .listener(new JobCompletionNotificationListener())
                .flow(step1())
                .end()
                .build();
    }

    @Bean
    public Step step1() {
        int CHUCK = 10;
        List<OrderDTO> orderList = TermDB.getAllOrder();

        return stepBuilderFactory.get("step1")
                .<OrderDTO, Order> chunk(CHUCK)
                .reader(new Job2Reader<>(orderList))
                .processor(new Job2Processor(createBy))
                .writer(new Job2Writer(orderRepository))
                .build();
    }

    @Override
    public void run() {
        log.info("MockJob2 Start - {}", new Date());
        try {
            JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis()).toJobParameters();
            jobLauncher.run(mockJob2(), jobParameters);
            log.info("MockJob2 Finish - " + new Date());
        }
        catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
