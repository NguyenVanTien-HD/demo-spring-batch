package com.tiennv.demospringbatch.entity;

import lombok.*;

import javax.persistence.*;
import java.time.Instant;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private Long customerId;

    private Long itemId;

    private String itemName;

    private Double itemPrice;

    private String purchaseDate;

    private Instant createDate;

    private String createBy;
}
