package com.tiennv.demospringbatch;

import com.tiennv.demospringbatch.dto.OrderDTO;

import java.util.ArrayList;
import java.util.List;

public class TermDB {
    public static List<OrderDTO> getAllOrder() {
        return new ArrayList<>() {
            {
                add(OrderDTO.builder().customerId(1L).itemId(1L).itemName("May tinh").itemPrice(30000.0).purchaseDate("2022/09/08 7:00:00").build());
                add(OrderDTO.builder().customerId(2L).itemId(2L).itemName("Ban phim").itemPrice(13000.0).purchaseDate("2022/09/08 5:00:00").build());
                add(OrderDTO.builder().customerId(3L).itemId(3L).itemName("chuot").itemPrice(220000.0).purchaseDate("2022/09/08 4:00:00").build());
                add(OrderDTO.builder().customerId(4L).itemId(4L).itemName("Quan ao").itemPrice(340000.0).purchaseDate("2022/09/08 3:00:00").build());
                add(OrderDTO.builder().customerId(5L).itemId(5L).itemName("Ao khoac").itemPrice(870000.0).purchaseDate("2022/09/08 2:00:00").build());
            }
        };
    }
}
