BatchProcessingApplication : Entrypoint của chương trình, chương trình sẽ bắt đầu chạy từ class này (Trong này có hàm main). Với việc đánh annotation @SpringBootApplication ta sẽ báo cho framework biết đây là 1 app spring boot và cần spring tự autoload các class config cần thiết khác mà cụ thể là BatchConfiguration.
    
BatchConfiguration : Như tên gọi, class này là mô tả cấu hình cho chương trình. Batch của chúng ta sẽ gọi đến các xử lý theo trình tự nào sẽ được mô tả ở đây.
    
(Job) importOrderJob : Ta có thể hiểu là 1 batch sẽ chạy nhiều job, và chương trình của mình đơn giản nên chỉ có 1 job duy nhất mô tả ở hàm này với ý nghĩa là import file csv vào database.
    
(Step) step1() : Trong 1 job sẽ có nhiều step, chương trình của mình đơn giản nên chỉ có 1 step tên là step1, thực tế ta có thể có nhiều step hơn.
    
(ItemReader) reader() : Mô hình cơ bản của 1 batch đó là "đọc input" --> "xử lý input" --> "xuất đầu ra", với flow 3 bước như vậy thì reader chính là bước đầu tiên "đọc input". Step1 được chia nhỏ hơn thành 3 sub step như vậy và sub step đầu tiên là reader.
    
(ItemProcessor) processor() : Đây là bước xử lý đầu vào từ reader(). Chương trình của mình hiện tại không có xử lý gì đặc biệt nhưng thực tế ta có thể có nhiều cách xử lý ví dụ như transform kiểu dữ liệu.
    
(ItemWriter) writer() : Sau khi dữ liệu được dữ liệu được xử lý xong, ta sẽ lưu chúng vào DB ở hàm này.
    
(Repository) OrderRepository : Trong Spring để lưu 1 Entity ta sẽ cần 1 wrapper bao chúng là Repository, OrderRepository chính là wrapper cần phải tạo này.
    
(Entity) Order : Đây là Entity thể hiện table ta cần lưu dữ liệu vào, trong trường hợp này vì nội dung cũng giống với từng field trong csv đầu vào nên class này cũng dùng chung để cả reader và Writer xử lý.